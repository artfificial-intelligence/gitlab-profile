# Artificial Intelligence

- [Generative AI for Beginners](https://medium.com/@raja.gupta20/generative-ai-for-beginners-part-1-introduction-to-ai-eadb5a71f07d)
- [DL Tutorials](https://gitlab.com/artfificial-intelligence/dl-tutorials)
- [LLM Tutorials](https://gitlab.com/artfificial-intelligence/llm-tutorials)
- [ML Tutorials](https://gitlab.com/artfificial-intelligence/ml-tutorials)

---
## Refrefences

- [FREE Tutorial Series — Python, ML, DL, NLP](https://medium.com/tech-talk-with-chatgpt/free-tutorial-series-17fefd0c4e07)
- [Generative AI for Beginners: Part 1 — Introduction to AI](https://medium.com/@raja.gupta20/generative-ai-for-beginners-part-1-introduction-to-ai-eadb5a71f07d) 2024-06-03
- [10분 만에 LangChain 이해하기](https://yozm.wishket.com/magazine/detail/2839/) 2024-11-14
- [10분 만에 RAG 이해하기](https://yozm.wishket.com/magazine/detail/2828/) 2024-11-14
- [뜨거운 감자, 'AI 에이전트'의 현재와 미래](https://yozm.wishket.com/magazine/detail/2895/?data=RhkCR7g6D5+t0gmscTmueXsDVAKDteO+KSj3zMoQdkc=&source=daily_latest_news)
- [경사 하강 알고리즘 - 심층 분석](https://artfificial-intelligence.gitlab.io/gradient-descent-algorithm/)
